use colored::Colorize;
use snafu::ResultExt;
use std::convert::TryFrom;
use std::io::{self, prelude::*};
use std::sync::mpsc::channel;
use std::{fs, path};
use threadpool::ThreadPool;

mod arg_parser;
mod constants;
mod env;
mod process;
mod types;

use env::Env;
use process::process_file;

const NUM_WORKERS: usize = 8;

fn init_out_dir(curr_dir: &path::PathBuf) -> types::Result<()> {
    let out_dir_path = curr_dir.clone().join(constants::OUT_DIR_PATH);

    if out_dir_path.is_file() {
        panic!("/out exists and is a file_path.");
    }

    if out_dir_path.is_dir() {
        fs::remove_dir_all(&out_dir_path).context(types::Generic)?;
    }

    fs::create_dir(out_dir_path).context(types::Generic)?;

    Ok(())
}

fn print_error(error: &types::Error) {
    println!("=> {}", format!("{}", error).red());
}

fn read_line_error_handler(
    (line_num, line): (usize, Result<String, std::io::Error>),
) -> (usize, String) {
    let line = line.context(types::Generic);

    if let Err(err) = line {
        panic!("{}", err);
    }

    (line_num, line.unwrap())
}

fn main() -> types::Result<()> {
    let args = arg_parser::get_args();
    let env = Env::try_from(args.value_of("env").unwrap())?;

    let curr_dir = std::env::current_dir().context(types::Generic)?;
    init_out_dir(&curr_dir)?;

    let files: Vec<String> = args
        .values_of("files")
        .unwrap()
        .map(|s| s.to_owned())
        .collect();
    let num_files = files.len();
    let threadpool = ThreadPool::new(NUM_WORKERS);

    let (tx, rx) = channel();

    for file_path in files {
        println!(
            "=> {}",
            format!("Processing file_path: \"{}\"", file_path).cyan()
        );

        let curr_dir = curr_dir.clone();
        let env = env.clone();
        let tx = tx.clone();
        threadpool.execute(move || {
            let file_name = path::Path::new(&file_path).file_name();
            let file_name: String = match file_name {
                Some(file_name) => file_name.to_string_lossy().to_string(),
                None => {
                    println!(
                        "=> {}",
                        format!("Invalid file name: \"{:?}\"", file_name).red()
                    );
                    tx.send(1).expect("Channel will be waiting for the pool");
                    return;
                }
            };
            let in_file_path = curr_dir.join(&file_path);
            let out_file_path = curr_dir.join(constants::OUT_DIR_PATH).join(&file_name);

            let mut file_errors: Vec<types::Error> = Vec::new();

            let in_file = fs::File::open(in_file_path).context(types::Generic);
            match in_file {
                Ok(in_file) => {
                    let mut in_reader_lines = io::BufReader::new(in_file)
                        .lines()
                        .enumerate()
                        .map(read_line_error_handler);

                    let out_file = fs::File::create(out_file_path).context(types::Generic);
                    match out_file {
                        Ok(out_file) => {
                            let mut out_writer = io::BufWriter::new(out_file);

                            let res = process_file(
                                &mut in_reader_lines,
                                &mut out_writer,
                                &file_path,
                                &mut file_errors,
                                &env,
                            );

                            if let Err(err) = res {
                                print_error(&err);
                            }
                        }
                        Err(err) => print_error(&err),
                    }
                }
                Err(err) => print_error(&err),
            };

            for error in file_errors.iter() {
                print_error(error);
            }

            tx.send(1).expect("Channel will be waiting for the pool");
        });
    }

    rx.iter().take(num_files).sum::<usize>();

    Ok(())
}
