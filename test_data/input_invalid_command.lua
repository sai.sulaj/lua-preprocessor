local one = 1;

--! IF
local dev_1 = "dev_1";
local dev_2 = "dev_2";

--! ELSE
local prod_1 = "prod_1";
local prod_2 = "prod_2";

--!

local two = 2;
