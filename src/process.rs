use iter_read::IterRead;
use snafu::ResultExt;
use std::convert::TryFrom;
use std::io;
use std::iter;
use std::str;

use crate::{constants, env::Env, types};

enum EnvState {
    Inside(Env),
    Outside,
    Always,
}
impl EnvState {
    pub fn keep_line(&self, env: &Env) -> bool {
        match *self {
            EnvState::Inside(ref self_env) => self_env == env,
            EnvState::Outside => true,
            EnvState::Always => false,
        }
    }

    pub fn set_env(&mut self, next: &Env) {
        *self = Self::Inside(next.clone());
    }

    pub fn swap_env(&mut self) {
        if let Self::Inside(ref prev_env) = *self {
            match prev_env {
                Env::Dev => *self = Self::Inside(Env::Prod),
                Env::Prod => *self = Self::Inside(Env::Dev),
            }
        }
    }
}

fn handle_env_command(
    line_iter: &mut str::SplitWhitespace,
    file_errors: &mut Vec<types::Error>,
    file_name: &str,
    line_num: &usize,
    line_raw: &str,
    env_state_ref: &mut EnvState,
) {
    let env_str = line_iter.next();
    if env_str.is_none() {
        let err = types::InvalidCommand {
            line_num: *line_num,
            file_name: file_name.to_owned(),
            command: line_raw.clone(),
        }
        .build();

        file_errors.push(err);
        return;
    }
    let env_str = env_str.unwrap();

    let next_env = Env::try_from(env_str);
    if let Err(err) = next_env {
        file_errors.push(err);
        return;
    };
    let next_env = next_env.unwrap();

    env_state_ref.set_env(&next_env);
}

fn handle_if(
    line_iter: &mut str::SplitWhitespace,
    file_errors: &mut Vec<types::Error>,
    file_name: &str,
    line_num: &usize,
    line_raw: &str,
    env_state_ref: &mut EnvState,
) -> bool {
    let command_str = line_iter.next();
    if command_str.is_none() {
        let err = types::InvalidCommand {
            line_num: *line_num,
            file_name: file_name.to_owned(),
            command: line_raw.clone(),
        }
        .build();

        file_errors.push(err);
        return false;
    }
    let command_str = command_str.unwrap();

    match command_str {
        "env" => handle_env_command(
            line_iter,
            file_errors,
            file_name,
            line_num,
            line_raw,
            env_state_ref,
        ),
        "always" => *env_state_ref = EnvState::Always,
        _ => {
            let err = types::InvalidCommand {
                line_num: *line_num,
                file_name: file_name.to_owned(),
                command: line_raw.clone(),
            }
            .build();

            file_errors.push(err);
        }
    };

    false
}

pub fn process_file(
    in_reader_lines: &mut dyn iter::Iterator<Item = (usize, String)>,
    mut out_writer: &mut dyn io::Write,
    file_name: &str,
    file_errors: &mut Vec<types::Error>,
    env: &Env,
) -> types::Result<()> {
    let mut env_state = EnvState::Outside;
    let env_state_ref = &mut env_state;

    let reader_filter = |(line_num, line_raw): &(usize, String)| -> bool {
        let line = line_raw.trim().to_lowercase();
        let mut line_iter = line.split_whitespace();

        let is_command = line_iter
            .next()
            .map(|s| s == constants::COMMAND_START)
            .unwrap_or(false);

        if !is_command {
            return env_state_ref.keep_line(env);
        }

        match line_iter.next() {
            Some(constants::IF) => handle_if(
                &mut line_iter,
                file_errors,
                file_name,
                line_num,
                line_raw,
                env_state_ref,
            ),
            Some(constants::ELSE) => {
                env_state_ref.swap_env();
                false
            }
            Some(constants::FI) => {
                *env_state_ref = EnvState::Outside;
                false
            }
            _ => {
                let err = types::InvalidCommand {
                    line_num: *line_num,
                    file_name: file_name.to_owned(),
                    command: line_raw.clone(),
                }
                .build();

                file_errors.push(err);
                return false;
            }
        }
    };

    let in_reader_filtered = in_reader_lines
        .filter(reader_filter)
        .map(|ns| {
            let mut line = ns.1;
            line.push('\n');
            line
        })
        .fuse();

    let mut byte_reader = IterRead::new(in_reader_filtered);

    io::copy(&mut byte_reader, &mut out_writer).context(types::Generic)?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::super::{env::Env, types};
    use super::process_file;
    use std::fs;
    use std::io::{self, prelude::*};

    #[test]
    fn test_dev() {
        let input = fs::File::open("test_data/input_valid.lua").unwrap();
        let expected_output = fs::read_to_string("test_data/output_dev.lua").unwrap();

        let input_reader = io::BufReader::new(input);
        let mut input_lines = input_reader.lines().map(|l| l.unwrap()).enumerate();

        let mut output = Vec::new();
        let mut output_writer = io::BufWriter::new(&mut output);

        let env = Env::Dev;

        let mut file_errors: Vec<types::Error> = Vec::new();
        let res = process_file(
            &mut input_lines,
            &mut output_writer,
            "test_file.lua",
            &mut file_errors,
            &env,
        );

        assert!(res.is_ok());
        assert_eq!(file_errors.len(), 0);

        drop(output_writer);
        let output = std::str::from_utf8(&output).unwrap();

        assert_eq!(output, expected_output);
    }

    #[test]
    fn test_prod() {
        let input = fs::File::open("test_data/input_valid.lua").unwrap();
        let expected_output = fs::read_to_string("test_data/output_prod.lua").unwrap();

        let input_reader = io::BufReader::new(input);
        let mut input_lines = input_reader.lines().map(|l| l.unwrap()).enumerate();

        let mut output = Vec::new();
        let mut output_writer = io::BufWriter::new(&mut output);

        let env = Env::Prod;

        let mut file_errors: Vec<types::Error> = Vec::new();
        let res = process_file(
            &mut input_lines,
            &mut output_writer,
            "test_file.lua",
            &mut file_errors,
            &env,
        );

        assert!(res.is_ok());
        assert_eq!(file_errors.len(), 0);

        drop(output_writer);
        let output = std::str::from_utf8(&output).unwrap();

        assert_eq!(output, expected_output);
    }

    #[test]
    fn test_invalid() {
        let input = fs::File::open("test_data/input_invalid_command.lua").unwrap();

        let input_reader = io::BufReader::new(input);
        let mut input_lines = input_reader.lines().map(|l| l.unwrap()).enumerate();

        let mut output = Vec::new();
        let mut output_writer = io::BufWriter::new(&mut output);

        let env = Env::Prod;

        let mut file_errors: Vec<types::Error> = Vec::new();
        let res = process_file(
            &mut input_lines,
            &mut output_writer,
            "test_file.lua",
            &mut file_errors,
            &env,
        );

        assert!(res.is_ok());
        assert_eq!(file_errors.len(), 2);
    }
}
