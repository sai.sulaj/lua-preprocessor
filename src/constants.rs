pub const COMMAND_START: &str = "--!";
pub const IF: &str = "if";
pub const DEV_ENV: &str = "dev";
pub const PROD_ENV: &str = "prod";
pub const ELSE: &str = "else";
pub const FI: &str = "fi";
pub const OUT_DIR_PATH: &str = "out";
