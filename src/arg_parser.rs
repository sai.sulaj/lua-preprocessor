use clap::{App, Arg, ArgMatches};

pub fn get_args<'a>() -> ArgMatches<'a> {
    App::new("Lua Pre-Processor")
        .version(clap::crate_version!())
        .author("Saimir Sulaj <hola@saimirsulaj.com>")
        .about("Simple Lua Pre-Processor")
        .arg(
            Arg::with_name("env")
                .short("e")
                .takes_value(true)
                .possible_values(&["dev", "prod"])
                .default_value("dev")
                .help("The environment"),
        )
        .arg(
            Arg::with_name("files")
                .takes_value(true)
                .multiple(true)
                .index(1)
                .required(true)
                .help("The list of files to process"),
        )
        .get_matches()
}
