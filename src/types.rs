use snafu::Snafu;

#[derive(Debug, Snafu)]
#[snafu(visibility = "pub")]
pub enum Error {
    #[snafu(display("Invalid env: {}", env))]
    InvalidEnv { env: String },

    #[snafu(display("Error: {}", source))]
    Generic { source: std::io::Error },

    #[snafu(display(
        "Invalid command at line {} in \"{}\": {}",
        line_num,
        file_name,
        command
    ))]
    InvalidCommand {
        line_num: usize,
        file_name: String,
        command: String,
    },
}

pub type Result<A = (), E = Error> = std::result::Result<A, E>;
