cargo build --release;

if [[ -f "/usr/local/bin/lua_prep" ]]; then
  rm /usr/local/bin/lua_prep;
fi

mv target/release/lua_prep /usr/local/bin;
