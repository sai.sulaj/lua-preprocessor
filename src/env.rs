use std::{cmp, convert};

use crate::{constants, types};

#[derive(cmp::PartialEq, Clone)]
pub enum Env {
    Dev,
    Prod,
}
impl convert::TryFrom<&str> for Env {
    type Error = types::Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value.to_lowercase().as_str() {
            constants::DEV_ENV => Ok(Self::Dev),
            constants::PROD_ENV => Ok(Self::Prod),
            other => types::InvalidEnv { env: other }.fail(),
        }
    }
}
impl std::fmt::Display for Env {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Env::Prod => write!(f, "{}", constants::PROD_ENV),
            Env::Dev => write!(f, "{}", constants::DEV_ENV),
        }
    }
}
